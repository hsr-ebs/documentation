package ch.hsr.ebos.example;

import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.logging.LoggingModule;
import ch.hsr.ebos.aion.logging.LogLevel;

public class LoggingExampleModule extends Module {
    @Override
    protected void setup() {
        // ....
        
        addModule(new LoggingModule(LogLevel.Info));
    }
}
