FROM ubuntu:16.04

RUN apt-get update && apt-get install -y texlive-latex-extra texlive-lang-german texlive-math-extra texlive-fonts-extra texlive-bibtex-extra biber texlive-science texlive-generic-extra