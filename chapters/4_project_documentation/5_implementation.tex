% !TEX spellcheck = de_DE
\documentclass[../../document.tex]{subfiles}

\begin{document}

\section{Implementation}
In den folgenden Unterkapiteln gehen wir auf die Implementationsdetails ein. 
Dabei wollen wir auf die wichtigsten Details und die dahinterliegenden Entscheidungen eingehen.

\subsection{Modulstruktur}
Die Implementation der einzelnen Packages wurde in separierten Java Modulen vorgenommen, welche sich gegenseitig referenzieren.
Dadurch ist die Funktionalität in viele kleine Module unterteilt, kann einzeln kompiliert und installiert werden.
Das Ziel ist es, die Abhängigkeiten für den Anwender zu reduzieren, so dass er nur die benötigten Module installiert.
Dies machte die Entwicklung aufwändiger, da wir die einzelnen Funktionen klar strukturieren und trennen mussten.
Aufgrund der Aufteilung werden die Schnittstellen besser geplant und umgesetzt.
Ausserdem können andere Entwickler mit geringerem Aufwand Erweiterungen und Integrationen an unserem Framework vornehmen.
\\\\
Um die Beziehungen der einzelnen Module zu zeigen, haben wir den resultierenden Abhängigkeitsbaum grafisch aufgearbeitet.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{aion/dependency-graph.png}
\caption{Modul-Abhängigkeitsdiagramm}
\label{fig:project-dependency-diagram}
\end{figure}

\newpage

\subsection{Timeline}
Die Idee hinter der Timeline war, dass eine Reihe an Events sich besser synchronisieren lassen als ein Datenzustand.
Einen Datenstand abzugleichen ist aufwändiger als nur die einzelnen Events durchzugehen, welche zu diesem führen.
Dieses Verfahren wird bei Datenreplikationen bereits eingesetzt, ist jedoch nicht für externe Teilnehmer einsehbar.\footnote{Turn the database inside out: \url{https://www.confluent.io/blog/turning-the-database-inside-out-with-apache-samza/}}

\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{data-replication.png}
\caption{Datenreplikation (turn the database inside out)}
\end{figure}

Die Events in einer Timeline können aus mehreren Datenoperationen bestehen und bilden eine Projection.
Sie können nicht parallel verarbeitet werden, da sie immer in der gleichen Reihenfolge abgespielt werden müssen.
Sollte dies zu Performanz Problemen führen, müssen die Events auf verschiedene Timelines aufgeteilt werden.

\subsection{Timeline Synchronisation}
Das Ziel der Timeline Synchronisation ist es, auf Client- sowie Serverseite dieselbe Reihenfolge an Events vorzufinden.
Die Client- und Server-Timeline können laufend durch neue Events erweitert werden, ohne Absprachen untereinander.
Sobald die Synchronisation beendet, sollten beide die gleiche Abfolge aufweisen.
Damit dies funktionieren kann, sind die Events unveränderbar und können auf der Serverseite nur angefügt werden.
Somit gibt der Server vor, welches die korrekte Reihenfolge ist, während der Client seine allenfalls korrigieren muss.
Dadurch ist der Aufwand auf der Serverseite gering.
Gleichzeitig stellen synchronisierende Clients keinen Mehraufwand dar.

Bei einer vollständigen Synchronisation werden in einem ersten Schritt alle lokalen Events, welche sich nicht bereits auf dem Server befinden, an den Server übermittelt (push).
In einem zweiten Schritt werden alle Events, welche sich noch nicht auf dem Client befinden, heruntergeladen (pull).
Die Reihenfolge push vor pull ist bewusst gewählt, denn dadurch kann die Synchronisationslogik vereinfacht und die Synchronisationszeit verkürzt werden.
Es verhindert, dass man in einem System, in welchem die Schreibkapazität sehr gross ist, endlos die Events herunterlädt.
Die Änderungen werden zum Server gesendet, sobald eine Verbindung besteht.
Dann holen wir die Informationen über den, zu diesem Zeitpunkt, letzten Event und laden alles zwischen der letzten Synchronisation und diesem herunter.

\subsection{Timeline Verarbeitung}
Die Events in einer Timeline können nicht ohne weiteres für Datenabfragen verwendet werden. 
Um zum Beispiel festzustellen, welche Projekte aktiv sind, müssten zuerst alle Events verarbeitet werden, damit ein Resultat zurück geliefert werden kann.
Dies ist sehr Ressourcenaufwändig.
Darum wird aus den Events eine Projection generiert, welche für Abfragen genutzt werden kann.
Der TimelineProcessor in Aion erzeugt diese zusammen mit der Business Logik.
Durch die Synchronisation kann es jedoch vorkommen, dass Events dazwischen eingefügt werden.
Der TimelineProcessor muss diese berücksichtigen, weil die Reihenfolge entscheidend sein kann.
Sobald ein Event eingefügt wird, muss der Processor an diese Stelle zurückspringen und die Projections neu berechnen.
Aus diesem Grund haben wir den CheckpointState entwickelt, welcher alle Datenmanipulationen speichert und diese zurücksetzen kann.

Ein anderer Lösungsansatz hätte sich mit Rückwärtsaktionen, für alle Events in der Business-Logik, beschäftigt.
Dies fanden wir allerdings umständlich für den Entwickler und auch fehleranfällig.

\subsection{Data OR-Mapper}
Weil wir die Kontrolle über alle Datenzugriffe benötigen, um die Datenmanipulationen abzufangen, entwickelten wir einen minimalen OR-Mapper.
Diese brauchen wir auch über das Transaktionsverhalten, um sicherzustellen, dass die Verarbeitung eines Events in einer einzigen Transaktion stattfindet.
Dadurch kommt es im Fehlerfall zu keinen Inkonsistenzen und die Verarbeitung kann zu jedem beliebigen Zeitpunkt unterbrochen werden.

Unser entwickelter OR-Mapper ist nicht ausführlich und es fehlt ihm an vielen üblichen OR-Mapper-Funktionalitäten.
Wir haben aus zeitlichen Gründen nur diejenigen umgesetzt, welche wir zwingend für unsere Offliss-Anwendung benötigten.

\subsection{Transaktionen}

In Aion müssen an mehreren Stellen Daten persistiert werden.
Dies sind Event Informationen, Projections und Synchronisationsfortschritte.
Für diese Schreibaktionen werden Datenbanktransaktionen verwendet, um sicherzustellen, dass Aion jederzeit unterbrochen werden kann und keine Dateninkonsistenzen entstehen.

\subsection{SQLite}
Die Persistenz auf Server und Client wurde mit SQLite umgesetzt, sodass die Implementation auf allen möglichen Geräten funktioniert.
Die Anbindung wurde mit JDBC entwickelt, was auf der Serverseite mit einer JVM kein Problem darstellte.
Für Android fanden wir ein Projekt, welches auch mit JDBC funktionierte, jedoch wies dieses einige Abweichungen zum Server auf.
Wir realisierten die Anwendungen so, dass der gleiche Code auf dem Server sowie dem Client verwendet werden kann.
Aufgrund von Problemen bei der Verwendung mehrerer Threads, die auf dieselbe Datenbank zugreifen, implementierten wir ein Locking, welches die gleichzeitige Nutzung der Connection verhindert.
Da dies in JDBC nicht standardmässig umgesetzt ist, entwickelten wir das selbst.

\newpage
\subsection{Aion Http-Server}
Der Aion Http-Server ist eine eigenständige Anwendung, die wir als Docker-Container ausliefern und verwenden.
Die Applikation wird durch eine Konfigurationsdatei gesteuert, welche die projektspezifischen Informationen beinhaltet.
Diese Datei ist in yaml spezifiziert.
Der Server soll wiederverwendbar und offen für andere Projekte sein.

\subsubsection{Konfiguration}
Die Konfiguration erlaubt es, mehrere Timelines zu spezifizieren.
In dem folgenden Beispiel aus dem Offliss, verwenden wir vier Timelines.
Dabei nutzen alle Timelines SQLite.
Eine davon ist mit einem Regex konfiguriert, welcher die Timeline separiert.
In Offliss wurden so die Zeitbuchungen pro Benutzer aufgeteilt, dadurch muss die Konfiguration nicht erweitert werden, sollte ein neuer Benutzer dazu kommen.

\lstinputlisting{sources/aion/http-server/configuration.yaml}

\newpage
\subsubsection{Autorisierung}
Ist eine Autorisierung konfiguriert, muss der Client bei jeder Abfrage seine Authentisierung mitsenden.
In unserer Anwendung haben wir uns für OAuth mit JWT entschieden, da JWT sehr gut offline verwendet werden kann.
In jeder Anfrage muss dadurch ein JWT Access Token enthalten sein, das Schreib- oder Leseberechtigungen hat.
Darin können Rechte und Metainformationen abgelegt werden, welche über eine Signatur sicher und ohne Kommunikation prüfbar sind.
Im folgenden Beispiel ist ein Ausschnitt eines JWT Access Tokens zu sehen, welcher definiert, dass der Aufrufer Schreib- und Leserechte auf seine eigene Timebooking- und Leserechte auf der User-Timeline besitzt.

\lstinputlisting{sources/aion/http-server/jwt-token.json}

Der Http-Server prüft diese Rechte zu Beginn der Anfrage und weisst unautorisierte Zugriffe zurück.
In jedem Timeline Event wird der ausführende Benutzer sowie seine derzeitigen Rollen erfasst.
Diese werden später in der Verarbeitung der Timelines verwendet, um sicherzustellen, dass einzelne Events nur von befugten Personen ausgeführt werden können.

Lese- und Schreibrechte können nur auf einer Timeline, und nicht auf Events definiert werden.
Berechtigungen auf Events können allerdings durch das AuthModule sichergestellt werden.

Keycloak implementiert OAuth 2.0 sowie OpenID, bietet eine Weboberfläche und eine API, um Benutzer und Konfigurationen zu verwalten.

In der Implementation nutzten wir den Passwort Grant und verzichteten auf den üblichen OAuth-Ablauf.
So konnten wir das Login direkt in der App umsetzen, ohne den kompletten OAuth/Ablauf zu realisieren.

\subsection{Logging}
Während der Entwicklung unserer Offliss Anwendung, stiessen wir des Öfteren auf das Problem, dass wir den Status der Events nicht feststellen konnten.
Das Debuggen war Zeitaufwändig und nicht immer möglich auf dem Server.
Wir haben darum im Laufe der Entwicklung ein Logging Modul geschrieben, welches uns die Verarbeitung und Resultate der Events auf der Konsole ausgibt.
\\\\
Das LogModule verwendet einen Interceptor, welcher vor die Aggregationen geschaltet wird und so nützliche Informationen ausgibt, wie zum Beispiel Event Informationen und Fehlermeldungen der Aggregatoren.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{aion/logging/logging-output.png}
\caption{LogModule Console Output}
\end{figure}

Es wurde bewusst nicht eine bestehende Logger-Bibliothek verwendet, um die Komplexität gering zu halten.
Bei der Implementation wurde aber darauf geachtet, dass die Ausgabe auf der Konsole durch eine Logger-Bibliothek ersetzt werden kann, welche diese in anderer Form abspeichern könnte.

\subsection{E-Mail Hosting}
Für den Versand von unseren Offliss E-Mails haben wir Mailgun verwenden.
Mailgun ist ein Dienstleister, welcher sich um den Versand von E-Mails kümmert.
Dadurch mussten wir keinen eigenen SMTP Server hosten.
Die Kosten von Mailgun sind relativ gering bzw. bis zu 10'000 E-Mails gratis.

\subsection{Offliss App}
Für jede Entität gibt es eine Listenansicht und eine detailliertere Ansicht zur Eingabe von Daten.

Für die Offliss App wurden eigene Komponenten verwendet, so dass sie dem Material Design für Android Apps entsprechen.

\subsubsection{Login}
Die Login-Ansicht erscheint als erstes, wenn die App neu installiert wird.
Man autorisiert sich mit seinem Benutzernamen und Passwort, die dann zur Überprüfung, via OAuth, an Keycloak weitergeleitet werden.
Ursprünglich wollten wir die E-Mailadresse als Benutzername verwenden, aber Keycloak erlaubt keine Anpassungen an diesem.
Deshalb fügten wir ein neues Attribut auf dem User hinzu, so dass sich die E-Mailadresse, bei einem Namenswechsel, anpassen lässt.

\newpage

Nach erfolgreichem Login bleibt man angemeldet.
Stimmen die Anmeldeinformationen nicht, wird der Benutzer daraufhin gewiesen.
Für die Anmeldung ist zwingend eine Verbindung zum Keycloak Service nötig.
Befindet sich der Benutzer offline und möchte sich anmelden, wird er mit einer entsprechenden Meldung informiert, dass das nicht geht.

\begin{figure}[h]
\centering
\begin{minipage}{.4\textwidth}
  \centering
  \includegraphics[width=1\textwidth]{offliss/user.png}
  \caption{Offliss Userliste}
\end{minipage}
\begin{minipage}{.4\textwidth}
  \centering
  \includegraphics[width=1\textwidth]{offliss/issue.png}
  \caption{Offliss Issue-Eingabe}
\end{minipage}
\end{figure}

\subsubsection{Overview Ansicht (User)}
Alle Listen beinhalten die wichtigsten Informationen über die Entitäten.
Im Beispiel der Benutzerübersicht wurden alle Informationen dargestellt.
\\
Viele Entitäten können aus Gründen der Nachvollziehbarkeit nicht entfernt, sondern nur inaktiviert werden.
In der Liste werden diese dann ausgegraut oder mit einem roten Status markiert.

\subsubsection{Detail Ansicht (Issue)}
Wird ein Eintrag angewählt gelangt man zur Eingabemaske.
Es gibt einige Entitäten, bei denen man gewisse Attribute nur einmal setzen kann.
Beispielsweise gehören der Benutzername des Users oder das Projekt und die Laufnummer der Issues dazu.

\subsubsection{Synchronization}
In der Synchronisation werden alle eigenen fehlgeschlagenen und laufenden Events dargestellt.
Das Datum zeigt, wann zuletzt eine Synchronisation durchlief.
Erfolgreich durchgeführte Events werden aus Platzgründen nicht dargestellt.
Die Synchronisation erfolgt alle 10 Sekunden, solange die App geöffnet ist.
Deshalb sieht man seine ausstehenden Events in den meisten Fällen nur, wenn man offline arbeitet.

\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{offliss/synchronisation.png}
\caption{Offliss Synchronisation}
\end{figure}


\subsubsection{Projection}
In der Offliss Anwendung wollten wir die Synchronisation verkürzen und nur einen Teil der Daten synchronisieren.
Deshalb haben wir die Events in vier Timelines aufgeteilt.
Dadurch können diese voneinander unabhängig synchronisiert und verarbeitet werden.
Allerdings wurden durch die Trennung der Timelines die Daten in verschiedene Datenbanken aufgeteilt.
Daraus resultierte, dass wir die Daten nicht in einer einzelnen SQL-Abfrage abrufen konnten.
Wir mussten darum die Daten der verschiedenen Projections jeweils einzeln nachladen, um alle benötigten Informationen im App darstellen zu können.

\subsection{Offliss Common}
Das Offliss Common Modul enthält die komplette Business Logik der Offliss Anwendung.
Dazu gehören alle Events und deren Aggregates, welche die Projections errechnen und Konfliktentscheidungen beinhalten.
Diese werden in der Android App sowie auf dem Server verwendet.
Die Event Aggregates sind leicht zu testen und werden mit den In-Memory Implementationen in den Unit Tests geprüft.

Aus Sicherheitsgründen haben wir absichtlich keine Passwörter in den Event-Daten gespeichert.
Diese würden ansonsten in der Timeline persistiert und für alle einsehbar sein.

\newpage

\subsection{Offliss Server}

Der Offliss Server ist zuständig für das Versenden von E-Mails sowie die Erstellung der Benutzer im OAuth Server.
Die Anwendung lädt regelmässig die Events aus den verschiedenen Timelines und tätigt die Aktionen, welche nicht auf dem App ausgeführt werden können.
Das Erstellen von Benutzer und das Versenden von E-Mails wird, aus Sicherheitsgründen, nur auf dem Server ausgeführt.
Der Server verwendet dazu die gleiche Business Logik wie in der App eingesetzt wird.

\subsection{Offliss Timelines}

Für unsere Offliss Anwendung mussten wir uns Gedanken machen wie die Timeline aussehen soll.
Die Grundlage dazu war die Definitionen der Events, welche wir im \autoref{chap:offliss-events} dokumentiert haben.

Wir erstellten anstelle einer einzigen grossen Timeline mehrere kleine.
Dies hat den Vorteil, dass wir später parallel und weniger Daten synchronisieren können.

\begin{figure}[H]
\centering
\includegraphics[height=6cm]{offliss/multi-timeline.png}
\caption{Timeline Aufteilung}
\label{fig:timeline-split}
\end{figure}

\paragraph{User Timeline}
Die User Timeline beinhaltet die Events CreateUser, ActivateUser, InactivateUser, UpdateUser sowie ResetUserPassword.
Hier werden die Benutzerinformationen gespeichert, welche für alle Anwender einsehbar sein müssen.
Der Entwickler und der Projektleiter können die Events nur lesen, während der Administrator diese erstellen kann.

\paragraph{Project Timeline}
Die Project Timeline beinhaltet die Events CreateProject, OpenProject, CloseProject und UpdateProject.
Hier befinden sich alle Projektinformationen aller Projekte.
Der Entwickler kann die Events lesen und der Projektleiter erstellen.

\paragraph{Issue Timeline}
Die Issue Timeline wird pro Projekt geführt und beinhaltet die Events CreateIssue, UpdateIssue, AssignIssue, UnassignIssue, CloseIssue und OpenIssue.
Die Entwickler und Projektleiter können hier lesen und schreiben.

\paragraph{TimeBooking Timeline}
Die TimeBooking Timeline wird pro Benutzer geführt und beinhaltet die Events CreateTimeBooking, UpdateTimeBooking und RemoveTimeBooking.
Jeder Entwickler kann nur in seine eigene Timeline schreiben und aus dieser lesen.

\subsection{Swagger UI}
Der Http-Server bietet eine URL an, welche die OpenAPI Spezifikation retourniert.
Damit kann zusammen mit dem Swagger UI eine grafische Oberfläche für die API dargestellt werden.
Diese nutzten wir in der Entwicklung relativ häufig zur Prüfung und Visualisierung der einzelnen Events.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{aion/http-server/swagger-ui.png}
\caption{Swagger UI}
\end{figure}

Die Implementation des Http-Clients wollten wir zuerst, anhand der OpenAPI Spezifikation, generieren.
Dann mussten wir jedoch feststellen, dass die generierten Datentypen Namenskonflikte verursachten.
Die gemeinsame Codebasis in unserem Projekt konnten wir nicht mit dem generierten Code zusammenführen.
Wir verzichteten darum auf die Generierung.

\newpage
\subsection{Unit Testing}
Viele Codeabschnitte in unseren Modulen sind mit Unit Tests abgedeckt.
Ausnahmen bilden die Module Android, App und der Offliss Server.
Diese wurden manuell durch unsere definierten Testfälle in \autoref{chap:manual-tests} getestet.
Für Aion erreichten wir eine durchschnittliche Testcoverage von 82\%.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{coverage/aion-coverage.png}
\caption{Code Coverage Aion}
\end{figure}

Im Offliss wurde nur die Business Logik mit Unit Tests geprüft, wobei wir auf 66\% Testabdeckung kamen.
Dieser tiefe Wert resultiert aus den vielen nicht verwendeten getter- und setter-Methoden in den Events, die einen grossen Teil der Zeilenanzahl ausmachen.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{coverage/offliss-coverage.png}
\caption{Code Coverage Offliss}
\end{figure}

\end{document}
