% !TeX spellcheck = de_DE
\documentclass[../../document.tex]{subfiles}

\begin{document}

\section{Einführung}
Die Offline-Synchronisation von Daten ist heutzutage trotz stark ausgebautem Mobilnetz eine wichtige Funktionalität, welche von vielen Anwendern in Auftrag gegeben oder ersehnt wird.
Obwohl die Mobilnetzabdeckung stetig erweitert wird, werden im Alltag eines mobilen Anwenders immer wieder Unterbrüche vermerkt.
Das Anwendererlebnis kann dadurch massgebend beeinträchtigt werden und die Bedienung verunmöglichen.
Ausserdem ist der Energieverbrauch der Geräte, bei der Kommunikation mit dem Mobilnetz, hoch und resultiert in einem rapiden Akkuverbrauch.
\\\\
Die Entwicklung von Offline-Anwendungen unterscheidet sich stark von Online-Anwendungen und ist mit mehr Aufwand und Planung verbunden.
Bei der Entwicklung mit Offline-Datenbanksystemen muss der Entwickler umdenken, da viele Funktionalitäten von relationalen Datenbanken in diesen Fällen nicht möglich sind.
Durch gute Planung können diese Limitationen frühzeitig erkannt und gelöst werden, jedoch können kleinste Anpassungen an der Anwendung zu Inkonsistenzen oder unzureichender Performanz führen.
\\
Eine weitere Herausforderung stellt die Testbarkeit von Synchronisationsproblemen dar.
Ein Fehler, welcher erst durch eine bestimmte Reihenfolge an Ereignissen eintritt, kann schwer zu entdecken sein.

Diese Arbeit ist in zwei Teile aufgeteilt.
Im ``Technischen Bericht`` gehen wir auf die Problemstellung, den aktuellen Stand der Technik bezüglich der Offline-Synchronisation sowie die Vision und die erarbeiteten Resultate ein.
Der zweite Teil, die Software-Projektdokumentation befasst sich mit Artefakten aus dem Software-Engineering-Vorgehen.

\subsection{Problemstellungen}
\label{chap:problems}
In der Entwicklung von offline-fähigen System treten Probleme auf, die denen von verteilen Systemen gleichen. 
Bevor wir uns in die Analyse von existierenden Lösungen vertiefen, wollen wir die Herausforderungen in den nachstehenden Kapiteln kurz erläutern und anhand von Beispielen veranschaulichen.

\subsubsection{Konfliktbehandlung}
\label{chap:problem-conflict}
Die Konfliktbehandlung ist eines der grössten Probleme in Offline-Anwendungen, wenn mehrere Anwender die gleichen Daten bearbeiten können.
Das Grundproblem liegt darin, dass wenn zwei Anwender offline den gleichen Datensatz verändern und diesen miteinander synchronisieren, ein Konflikt entsteht.
Es gibt verschiedene Strategien diesen Konflikt aufzulösen:

\paragraph{Letzte Synchronisation gewinnt}
Der Anwender, welcher seine Änderungen dem Server zuletzt kommuniziert hat, gewinnt.
Synchronisiert Anwender 1 Daten, welche Anwender 2 zuvor bereits verändert hat, werden die Änderungen von Anwender 1 behalten und die von Anwender 2 überschrieben.
Diese Strategie ist einfach zu realisieren und performant, birgt jedoch Gefahren.
Eine der Grössten stellt ein Anwender dar, der eine lange Zeit seine Daten nicht synchronisiert hat und somit alte Datenstände in der Datenbank besitzt.
Sobald dieser seine Daten auf den Server lädt, werden alle Änderungen in der Zwischenzeit von diesem Anwender überschrieben.

\paragraph{Erste Synchronisation gewinnt}
Mit dieser Strategie darf der Anwender, welcher zuerst seine Änderungen dem Server kommuniziert, seine Daten behalten.
Synchronisiert Anwender 1 Daten, welche Anwender 2 zuvor bereits verändert hat, gehen die Änderungen von Anwender 1 verloren und die von Anwender 2 werden festgeschrieben.
Für diese Strategie muss auf allen Datensätzen eine Version geführt werden, zur Erkennung von Konflikten, auch bekannt unter \Gls{OCC}.

\paragraph{Zusammenführung}
Änderungen von verschiedenen Anwendern werden bei dieser Strategie zusammengeführt. 
Dabei werden die einzelnen Datenfelder eines Datensatzes verglichen und in einer Version, mit den Änderungen von beiden Anwendern, gespeichert.
Sollten die beiden Anwender unterschiedliche Datenfelder verändert haben, werden beide Änderungen gespeichert, so dass keine verloren geht.
Wenn einige Datenfelder von beiden Anwendern bearbeiten wurden, muss entschieden werden welcher Anwender bevorzugt wird.
Da eine Logik implementiert werden muss, welche Entscheidungen aufgrund der einzelnen Datensätze vornimmt und diese speichert, ist diese Strategie besonders aufwändig zu realisieren.
Diese Logik ist spezifisch auf einen Anwendungsfall ausgerichtet und kann nicht für alle Arten von Daten gleich implementiert werden.

\subsubsection{Berechtigung}
Die Berechtigung von Daten kann für Anwendungen entscheidend sein.
Es gibt Daten und Aktionen, die nur von Anwendern in einer bestimmten Rolle eingesehen oder getätigt werden können.
In einer klassischen Server-Client-Architektur könnte eine Serveranwendung diese Berechtigungen prüfen und das Resultat dem Client mitteilen.
Dies ist in einer Anwendung, ohne Verbindung zu einer Serverinstanz nicht möglich.
Alle Aktionen, welche die Anwendung offline anbieten will, müssen auf dem Gerät implementiert und in einer lokalen Datenbank gespeichert werden.
Manipulationen am Client können von Angreifern ausgenutzt und nicht vollständig verhindert werden. 
Deshalb sollten die Änderung der lokalen Datenbank nicht ohne Prüfung von der Serverseite übernommen werden.

\subsubsection{Transaktionen}
Einige Anwendungen haben Aktionen, welche Änderungen an verschiedenen zusammenhängenden Datensätzen durchführen und sollten deshalb in einer isolierten Transaktion ausgeführt werden.
Diese Gruppierung von Änderungen in einer Transaktion müssen auch bei einer Synchronisation berücksichtigt werden, da ansonsten diese verletzt werden kann.
In vielen dokumentenbasierten Datenbanken ist dieses Problem vorhanden und wird durch das Wählen einer geeigneten Datenstruktur umgangen. 
Eine zu wählen, die zu einer möglichst konfliktfreien Synchronisation führt sowie transaktionsunabhängig ist, kann schwierig oder gar unmöglich sein. 

\subsubsection{Einmaliges Vorkommen}
Manche Daten dürfen im System nicht doppelt vorkommen, zum Beispiel E-Mailadressen von Anwendern. 
Um diese Anforderung einhalten zu können, muss in einer Offline-Anwendung sichergestellt werden, dass nach einer Synchronisation diese Regeln nicht verletzt sind.
Werden diese verletzt, muss dagegen etwas unternommen werden.

\subsubsection{Sequenzen}
Um beispielsweise eine chronologische Bestellnummer in einer Offline-Anwendung zu führen, muss nach erfolgreicher Datensynchronisation die Laufnummer hochgezählt werden.
Diese Laufnummer wird erst nach dem Synchronisationszeitpunkt ermittelt und muss bis zu diesem Zeitpunkt versteckt oder angenähert werden.

\subsubsection{Unterbruchsfreie Bedienung}
Eine weitere Schwierigkeit einer Offline-Anwendung stellt die unterbruchsfreie Bedienung der Anwendung dar.
Die Datensynchronisation kann jederzeit im Hintergrund ausgelöst werden und darf den Anwender nicht in der Bedienung seiner Tätigkeit unterbrechen. 

\subsubsection{Teil-Synchronisation}
Nicht alle Daten einer Datenbank müssen jederzeit auf den mobilen Geräten verfügbar sein.
In vielen Fällen können auf diesen nur limitierte Datenmengen gespeichert werden. 
Ausserdem ist die Rechenleistung der Geräte für grosse Mengen nicht ausreichend.
Durch eine Selektion der Daten, werden diese eingeschränkt und bewirken dadurch kürzere Synchronisationszeiten.

\subsubsection{Externe Systeme}
Über Schnittstellen sind Anwendungen oft mit anderen Systemen verbunden. 
Die Anwendung muss damit umgehen können, dass diese nicht jederzeit verfügbar sind.
Alle Anwenderaktionen müssen deshalb auch ohne diese Systeme auskommen und die Kommunikation später nachholen können.
Ein praktisches Beispiel wäre eine E-Mail zu versenden, sobald ein Datensatz bearbeitet wurde.
Damit diese Aktion offline funktionieren kann, muss die E-Mail versendet werden können, sobald die Anwendung Verbindung zum E-Mailserver aufbauen konnte.

\subsubsection{Daten-Migrationen}
Anwendungen sollen stetig weiterentwickelt werden können.
Daraus entstehen verschiedene Versionen.
Diese können Datenstrukturen erweitern oder verändern, um neue Funktionalitäten anzubieten.
Eine alte Version einer Anwendung kann unsynchronisierte Änderungen besitzen, welche verloren gehen können, wenn der Anwendungsserver die alte Version nicht mehr versteht und behandeln kann.

\subsubsection{Anwendungsoberfläche}
Anwendungen müssen Aktionen ausführen und die resultierenden Veränderungen darstellen.
Diese können bis zum Synchronisationszeitpunkt nicht mit Sicherheit bestätigt werden.
Im Falle eines Konflikts, welcher nicht aufgelöst werden kann, muss der Anwender interagieren können oder informiert werden, dass seine getätigte Aktion nicht erfolgreich war.
Der Anwender sollte einen Überblick haben über ausstehende Aktionen und Änderungen, damit er erkennen kann, ob diese für andere Benutzer sichtbar sind oder er zuerst eine Datensynchronisation durchführen muss.

\subsubsection{Performanz}
\label{chap:problem-perf}
Zur Performanz gibt es verschiedene Aspekte, die bei einer Offline-Anwendung wichtig sein können.

\paragraph{Initiale Datensynchronisationszeit}
Die erste Datensynchronisation ist die, bei welcher die meisten Daten vom Server auf den Geräten landen.
Dies kann eine hohe Last auf der Serverseite erzeugen, falls dieser Fall nicht optimiert wurde.
Der Anwender kann dadurch blockiert werden, weil er die Anwendung erst bedienen kann, wenn sich alle benötigten Daten auf seinem Gerät befinden. 

\paragraph{Synchronisationszeit}
Eine Anwendung sollte möglichst oft synchronisieren, damit alle Anwender eine gleiche Datenbasis besitzen.
Die Synchronisation sollte darum so kurz und mit so wenig Datenaustausch wie möglich funktionieren.
Im Idealfall ist die Synchronisationszeit nicht von der Anzahl der Datensätze abhängig, sondern von den veränderten Datensätzen, welche synchronisiert werden.

\paragraph{Serverauslastung}
Auf dem Server muss die Auslastung verteilbar sein, damit die Synchronisationszeit bei ansteigender Nutzerzahl nicht zunimmt.
Der Verarbeitungsdurchsatz synchronisierter Datensätze sollte bei steigender Anzahl Nutzer konstant bleiben.
Ausserdem dürfen unter den verschiedenen Anwender einzelne nicht bevorzugt werden.

\paragraph{Gerätauslastung}
Während einer Synchronisation darf die Rechenleistung nur soweit genutzt werden, dass der Anwender davon nichts merkt.
Dabei sollen die Kommunikation und der Rechenaufwand auf einem Minimum gehalten werden, damit der Akku des mobilen Geräts nicht beeinträchtigt wird.

\subsection{Vorteile von Offline-Lösungen}
\paragraph{Unterbruchsfrei}
Die Anwendung kann immer und jederzeit bedient werden, unabhängig von ihrer Verbindungsqualität.

\paragraph{Schnellere Abfragen}
Da sich die Daten lokal auf dem Gerät befinden, verkürzt sich die Latenz erheblich, weil keine direkte Kommunikation zum Server stattfinden muss.

\paragraph{Datenmenge}
Bei regelmässiger Nutzung wird die Datenmenge in der Kommunikation geringer, da nur die Veränderungen übermittelt werden. 

\paragraph{Dezentralisierung}
Die mobilen Geräte funktionieren eigenständig und benötigen keinen stetig erreichbaren Server. 
Dadurch wird die Verfügbarkeit der Anwendung gesteigert, ohne die des Servers zu erhöhen.

\subsection{Vision}
Mit dieser Arbeit wollen wir aufzeigen, dass obwohl Offline-Anwendungen komplizierte Problematiken beinhalten, diese lösbar sind.
Die Entwicklung dieser Anwendung muss vereinfacht werden.
Der Fokus der Softwareentwickler soll bei den Kernproblemen der Business-Domäne liegen und nicht bei der Synchronisation der benötigten Daten.
\\\\
Die erarbeitete Lösung soll sich nicht nur auf eine Art von Anwendung beschränken, sondern generisch in vielen Problembereichen eingesetzt werden können.
Uns ist bewusst, dass unser Produkt nicht alle Bedürfnisse aller Anwender befriedigen kann. Deshalb legen wir grossen Wert auf vielfältige Konfigurationsmöglichkeiten und Erweiterbarkeit.
Wir streben eine einfache, unkomplizierte Lösung an.
\\\\
Unser Framework soll nicht wie andere Systeme über den Datenzustand (Projection), sondern über eine Änderungsliste synchronisiert werden.
Dabei werden einzelne Ereignisse (Events) in einer nicht veränderbaren Liste gespeichert.
Der aktuelle Zustand kann durch diese Reihe an Ereignissen errechnet und persistiert werden, dient aber nicht als Grundlage zur Replikation.
\\\\
Das Resultat dieser Arbeit soll frei zugänglich sein.
Wir vermuten, dass andere Entwickler und Unternehmen dieselben Problemstellungen wie wir haben.
Darum möchten wir dieses Projekt unter einer Open Source Lizenz veröffentlichen.
Bei Projekterfolg wäre die Open Source Community eine gute Bereicherung an Rückmeldungen und Unterstützung.

\newpage

\subsection{Ziele und Unterziele}
\paragraph{Problemstellungen}
Wir möchten möglichst viele Problemstellungen in der Entwicklung von Offline-Anwendungen aufzeigen.
Dies soll uns helfen, eine Denkweise für Offline-Anwendungen zu bekommen.

\paragraph{Spezifikation eines Anwendungsbeispiels} 
Es soll eine Spezifikation eines Offline-Anwendungsbeispiels mit einem praktischen Nutzen in der realen Welt definiert werden.
Zu diesem Anwendungsbeispiel werden die Anforderungen so gewählt, dass alle erarbeiteten Problemstellungen vorkommen.
Die Anwendung soll in Java entwickelt und auf einem Android Gerät installiert werden können.

\paragraph{Analyse}
Anhand der erarbeiteten Problemstellungen, möchten wir die existierenden Lösungen analysieren. 
Das Fazit wird entscheiden, wie das Anwendungsbeispiel umgesetzt wird und ob eine Entwicklung eines eigenen Frameworks sinnvoll ist.

\paragraph{Offline-Framework entwickeln} 
Stellt sich heraus, dass keine der existierenden Lösungen die definierten Probleme vollumfänglich lösen, werden wir unser eigenes Framework planen und entwickeln. 
Dieses wird nicht anwendungsspezifisch umgesetzt, sondern offen für Erweiterungen. 
Wie das Anwendungsbeispiel soll auch das Framework in Java entwickelt werden und aus einer Client- sowie Serverkomponente bestehen.
Als Linux Container wird die Serverkomponente ausgeliefert.
Die Entwicklung soll unter der MIT Open-Source Lizenz stattfinden.

\paragraph{Dokumentation} 
Erfolgserlebnisse sowie Fehlschläge werden dokumentiert. 
Die Dokumentation soll anderen einen Einblick in die Probleme und Lösungen der Offline-Entwicklung geben.

\end{document}