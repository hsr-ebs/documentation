#!/bin/sh

pdflatex -interaction=nonstopmode -halt-on-error $1
biber $1
makeglossaries $1
pdflatex -interaction=nonstopmode -halt-on-error $1
pdflatex -interaction=nonstopmode -halt-on-error $1