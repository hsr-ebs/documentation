param
(
  $Document = 'document.tex'
)

pdflatex -interaction=nonstopmode -halt-on-error $Document
biber $Document
makeglossaries $Document
pdflatex -interaction=nonstopmode -halt-on-error $Document
pdflatex -interaction=nonstopmode -halt-on-error $Document