#!/bin/sh

extensions=( aux bbl blg fdb_latexmk fls ilg lof log lol lot nlo nls out synctex.gz tdo toc idx ind run.xml anc acr acn alg bcf gls glo glg ist )

FOLDER='.'

for ext in ${extensions[@]} ; do
    echo "find -delete $ext"
    find $FOLDER -name "*."$ext -type f -delete
done


