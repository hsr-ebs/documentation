# Documentation

## Build

### macOS
```
./build_latex.sh
```

### Windows
```powershell
.\build_latex.ps1
```

## Clean

### macOS
```
./remove_latex_output_files.sh
```

### Windows
```powershell
.\remove_latex_output_files.ps1
```

## Artifacts

* [Latest document](https://gitlab.com/hsr-ebs/documentation/-/jobs/artifacts/master/raw/document.pdf?job=build)
* [Latest reduced document](https://gitlab.com/hsr-ebs/documentation/-/jobs/artifacts/master/raw/document_reduced.pdf?job=build)
